require "mediawiki_api"
require 'sqlite3'
require 'pg'

# prepare wiki connection
wiki_client = MediawikiApi::Client.new "http://127.0.0.1:81/api.php"
wiki_client.log_in "Stoev.Aleksandar", "mynewkatjapasword"

# prepare to insert relational data

pg_conn = PG.connect("127.0.0.1", 5432, '', '', "katja", "postgres", "geslojeskritokatja")

files_path = "/run/media/astoev/Data/alex/kartoteka/ivotachev/gkpb/htdocs"

def to_coordinate(str)
    if str.nil?
        return nil
    elsif str.class.equal? Float
        return str
    elsif str.length == 0
        return nil
    elsif str.include? '*'
        puts "Drop coordinate ${str}"
        return nil
    else
        coord = str.tr('*', '"').tr('`', '\'').tr(',', '.');
        puts str + " -> " + coord
        return coord;
    end
end

# content of the page
def get_template(row, colnames, files, od, to)
  return "

[[Category:Добростан]]

{{Objects}}

===== Описание =====

   <pre>#{od}</pre>

  ''' Достъп: ''' #{row[colnames.index 'GPS_NOTES']}, #{row[colnames.index 'ZAB']}

  ''' Особености: ''' #{row[colnames.index 'OSOB']}


===== История на проучването =====
#{row[colnames.index 'OTKRIL']}

  {{Template:List of actions for object}}

===== Техническо описание =====
  <pre>#{to}</pre>

===== Special features =====
Here we enter special and distinctive features of object.

===== Pollution =====
Describe pollution of object.

===== Геология =====
 '''Скали: '''#{row[colnames.index 'ROCK']}

 '''Седименти: '''#{row[colnames.index 'SEDI']}

 '''Тектоника: '''#{row[colnames.index 'TECT']}

 '''MNT: '''#{row[colnames.index 'MNT']}

 '''Релеф: '''#{row[colnames.index 'RELEF']}

 '''Форми: '''#{row[colnames.index 'FORMS']}


===== Биология =====
#{row[colnames.index 'BIO']}
#{row[colnames.index 'BIO_NOTES']}

===== Хидрология =====
#{row[colnames.index 'HYDROGEO']}

===== Археология =====
All very old things.

===== Further research =====
Hints and tips for further research

===== Документи =====
{{Template:List of KATJA documents}}

{{Template:List of documents}}

{{Template:Image gallery}}

===== Източници и литература =====

---
#{files}
---

===== Varia =====
===== Full data =====
pagename={{#external_value:pagename}} ,
image={{#external_value:image}} ,
natcatnumb={{#external_value:natcatnumb}} ,
clubcatnumb={{#external_value:clubcatnumb}} ,
code={{#external_value:code}} ,
name={{#external_value:name}} ,
nattype1={{#external_value:nattype1}} ,
nattype2={{#external_value:nattype2}} ,
x={{#external_value:x}} ,
y={{#external_value:y}} ,
elevation={{#external_value:elevation}} ,
vlocatedby={{#external_value:vlocatedby}} ,
vaccess={{#external_value:vaccess}} ,
vnatregstatus={{#external_value:vnatregstatus}} ,
mapsymbol={{#external_value:mapsymbol}} ,
accuracy={{#external_value:accuracy}} ,
geogunit1={{#external_value:geogunit1}} ,
geogunit2={{#external_value:geogunit2}} ,
geogunit3={{#external_value:geogunit3}} ,
length={{#external_value:length}} ,
depth={{#external_value:depth}} ,
measured={{#external_value:measured}} ,
discoverydate={{#external_value:discoverydate}} ,
author={{#external_value:author}} ,
organization={{#external_value:organization}} ,
comment={{#external_value:comment}} ,
condition={{#external_value:condition}} ,
entrycomment={{#external_value:entrycomment}} ,
lasteditor={{#external_value:lasteditor}} ,
created_at={{#external_value:created_at}} ,
locationcomment={{#external_value:locationcomment}} ,
surface={{#external_value:surface}} ,
volume={{#external_value:volume}} ,
sync_level={{#external_value:sync_level}},
sync_mask={{#external_value:sync_mask}},
sync_status={{#external_value:sync_status}},
research_level={{#external_value:research_level}}

{{Template:List of objects near by|coordinatesare={{#show: {{PAGENAME}} | ?Has position }}}}
"
end

def read_text(path, id, type)

  txtFile = "#{path}/#{type}/#{id}_#{type}.txt"
  unless !File.file?(txtFile)
    puts "Adding #{type} from #{txtFile}"
    return File.open(txtFile,  'r:windows-1251:utf-8').read
  end
  return ""
end

def upload_image(wiki_client, path, id, type, ext, comment, name_override = nil)
  image_name = "#{id}_#{type}.#{ext}"
  image = "#{path}/#{image_name}"
  unless !File.file?(image)
    if (!name_override.nil?)
      image_name = "#{id}_#{name_override}.#{ext}"
    end

    puts "Adding image #{image} as #{image_name}"
    wiki_client.upload_image(image_name, image, comment, true)
    if (name_override.nil?)
      return ""
    end
    return "\n[[File:#{image_name}|600px|thumb|center|#{comment}]]"
  end

  return "";
end

# start reading the sqlite database
counter = 0



sqlite_conn = SQLite3::Database.open("pesteri")
query = 'SELECT * FROM PEST p join info i on p.d_id = i.d_id
    join gis g on g.d_id = p.d_id';
colnames = sqlite_conn.prepare(query).columns
puts colnames
sqlite_conn.execute query do |row|
    counter+=1

    puts "#{counter}: #{row}"


    id = row[colnames.index 'D_ID']
    cave_name = row[colnames.index 'NAME']
    lat = to_coordinate(row[colnames.index 'X'])
    lon = to_coordinate(row[colnames.index 'Y'])
    elevation = row[colnames.index 'Z']
    elevation = 0 unless elevation.nil?
    length = row[colnames.index 'LEN']
    length ||= 0
    depth = row[colnames.index 'BOT']
    if depth.nil? || depth==""
        depth = 0
    end
    location_text = "POINT(#{lat} #{lon})"
    location_text = "POINT(0 0)" unless !lat.nil?

    files = ""
    od = ""

    # files
    image_name = "#{id}_image.jpg"
    files = files + upload_image(wiki_client, "#{files_path}/FOTO", id, 'FV', 'jpg', 'Снимка на входа', 'image')
    files = files + upload_image(wiki_client, "#{files_path}/GMO", id, 'GMO', 'png', 'Описание на достъпа')
    files = files + upload_image(wiki_client, "#{files_path}/SR", id, 'SR', 'png', 'Кроки')
    files = files + upload_image(wiki_client, "#{files_path}/TO", id, 'TO', 'png', 'Техническо описание')

    # description from text files
    od = read_text(files_path, id, 'OD')
    toTxt = read_text(files_path, id, 'TO')

    template = get_template(row, colnames, files, od, toTxt)
    name = "#{id}_#{cave_name}";


    # biblio = sqlite_fetch_array(sqlite_unbuffered_query($db_id, "select count(D_ID) from ID2BIBLIO where D_ID=$ID;"));
    #
    # $sql = "SELECT BIBLIO.*,ID2BIBLIO.* FROM ID2BIBLIO,BIBLIO WHERE ID2BIBLIO.D_ID=$ID and ID2BIBLIO.M3=BIBLIO.M3;";
    # $qrez=sqlite_unbuffered_query($db_id,$sql);
    # while ($bibdata = sqlite_fetch_array($qrez)) {
    #     $y=$y + 1;
    # echo "<br><font color=blue>$y. $bibdata[4] $bibdata[5] $bibdata[2] $bibdata[3] $bibdata[9] $bibdata[6] $bibdata[10] áð.$bibdata[11] $bibdata[13] $bibdata[12]; Çàá.:  $bibdata[23]</font>";
    # }
    # }
    #
    #

    # db form
    puts "Add to DB #{id}"
    # begin
        insert_objects_query = 'INSERT INTO OBJECTS (pagename, geom, natcatnumb, code, length, depth, elevation, isvalid, image)
           values ($1, (SELECT ST_GeomFromText($2, 4326)), $3, $4, $5, $6, $7, true, $8)'
        insert_objects_parms = [name, location_text, id, "CODE", length, depth, elevation, image_name]
        puts "Insert #{insert_objects_query} with #{insert_objects_parms}"
        pg_conn.exec_params(insert_objects_query, insert_objects_parms)
    # rescue StandardError => e
    #     puts "DB insert failed"
    #   puts e
    # end

    # the page
    wiki_client.create_page name, template

    break unless counter < 80
  end


